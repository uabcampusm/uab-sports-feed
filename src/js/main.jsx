var React = require("-aek/react");
var Container = require("-components/container");
var {VBox,Panel} = require("-components/layout");
var {BannerHeader} = require("-components/header");
var {AekReactRouter,RouterView} = require("-components/router");

var IndexPage = require("./pages/index");
var SportPage = require("./pages/sports");

var router = new AekReactRouter();

var Screen = React.createClass({

  selectSport:function(sport){
    router.goto("/feed/" + sport);
  },

  render:function() {

    return (
      <Container>
        <VBox>
          <BannerHeader theme="alt" key="header" flex={0}>UAB Sports</BannerHeader>
          <RouterView router={router}>
            <IndexPage path="/" onSelect={this.selectSport} />
            <SportPage path="/feed/:sport" />
          </RouterView>
        </VBox>
      </Container>
    );

  }

});

React.render(<Screen/>,document.body);
