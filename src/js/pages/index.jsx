var React = require("-aek/react");
var Page = require("-components/page");
var {BasicSegment} = require("-components/segment");
var {Listview} = require("-components/listview");

var IndexPage = React.createClass({
  onClick: function(item,ev){
    ev.preventDefault();
    var code = item.Code;
    console.log(code);
    this.props.onSelect(code);
  },

  render:function(){
    var sports = [
                    {text:"Baseball",Code:"m-basebl"},
                    {text:"Softball",Code:"w-softbl"},
                    {text:"Men's Basketball",Code:"m-baskbl"},
                    {text:"Women's Basketball",Code:"w-baskbl"},
                    {text:"Men's Football",Code:"m-footbl"},
                    {text:"Men's Golf",Code:"m-golf"},
                    {text:"Women's Golf",Code:"w-golf"},
                    {text:"Men's Soccer",Code:"m-soccer"},
                    {text:"Women's Soccer",Code:"w-soccer"},
                    {text:"Men's Tennis",Code:"m-tennis"},
                    {text:"Women's Tennis",Code:"w-tennis"},
                    {text:"Women's Beach Volleyball",Code:"w-svolley"},
                    {text:"Women's Bowling",Code:"w-bowl"},
                    {text:"Women's Cross-Country",Code:"w-xc"},
                    {text:"Women's Rifle",Code:"c-rifle"},
                    {text:"Women's Track",Code:"w-track"},
                    {text:"Women's Volleyball",Code:"w-volley"}
                 ];
    return (
      <Page>
        <BasicSegment>
          <Listview items={sports} uniformLabels basicLabel onClick={this.onClick} />
        </BasicSegment>
      </Page>
    );
  }
});

module.exports = IndexPage;
