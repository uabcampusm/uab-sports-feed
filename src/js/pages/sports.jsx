var React = require("-aek/react");
var Page = require("-components/page");
var {BasicSegment,Segment} = require("-components/segment");
var Group = require("@ombiel/aek-lib/react/components/group");
var {Button} = require("-components/button");
var {Header} = require("-components/header");
var request = require("-aek/request");
var {Listview, Item} = require("-components/listview");
var {InfoMessage} = require("-components/message");
var moment = require("moment");

var SportsPage = React.createClass({
  getInitialState: function(){
    return {
      loading: true,
      error: false
    };
  },

  componentDidMount: function(){
    this.getData();
  },

  getData:function(){
    var ctx = this.props.ctx;
    var code = ctx.params.sport;

    request.action("get_feed").send({code:code}).end((err,res)=>{
      console.log(res);
      console.log(err);
      if(!err && res){
        var result = res.body.channel;
        this.setState({result: result, loading:false, error: false});
      }else{
        this.setState({error: true, loading: false});
      }
    });

  },

  render:function() {
    var loading = this.state.loading;
    var result = this.state.result;
    var error = this.state.error;
    var errorMsg;
    var content,title,image,items;
    if(error){
      errorMsg = <InfoMessage heading="Sorry">We currently can't get the information for this sport. Please try again later.</InfoMessage>;
    }

    if(!loading && result){
      console.log(result);
      title = result[0].title[0]["@text"];
      image = result[0].image[0].url[0]["@text"];
      items = result[0].item;
      content = [
        <span>
          <Header level="2" dividing image={image}>{title}</Header>
          <Listview className="noshadow" items={items} itemFactory={(item)=>{
            var title = item.title[0]["@text"];
            var desc = item.description[0]["@text"];
            var date = moment(item.pubDate[0]["@text"]).format("dddd, MMMM DD, YYYY");
            var link = item.link[0]["@text"];
            return (
              <Item>
                <Group>
                  <Header subtext={date} level="3">{title}</Header>
                  <Segment dangerouslySetInnerHTML={{__html: desc}} />
                  <Button fluid href={link} variation="alt">Read More</Button>
                </Group>
              </Item>
            );
          }}/>
        </span>
      ];
    }

    return (
      <Page>
        <BasicSegment loading={loading}>
          {content}
          {errorMsg}
        </BasicSegment>
      </Page>
    );
  }

});

module.exports = SportsPage;
